# Faille applicative Tomcat : CVE-2016-5425

## Contenu du repo

* **Dockerfile**
* **exploit.sh** : script d'exploitation de la faille.
* **simulate_reboot_script.sh** : script de simulation du redémarrage d'une machine se connectant en tant que root pour exécuter la commande *systemd-tmpfiles --create*.
* *rapport.pdf*

## Prérequis

- Installer [Docker](https://www.docker.com)

## Procédure de test

1. Construire une image docker à partir du Dockerfile :

		docker build -t ensimag/cve_tomcat:1 .

2. Lancer un conteneur docker depuis cette nouvelle image :

		docker run -d --name cve_tomcat -p 4242:22 ensimag/cve_tomcat:1

3. Et maintenant vous pouvez vous connecter en ssh au conteneur avec l'utilisateur tomcat_user et le mot de passe 'motdepasse':

		# le mot de passe est 'motdepasse'
		ssh tomcat_user@localhost -p 4242

4. Une fois sur la machine vous allez trouver dans le home de l'utilisateur, un fichier 'exploit.sh', exécutez le.

		./exploit.sh
		# L'exploit en lui-même est expliqué dans la documentation.

5. Vous pouvez vous déconnecter et lancer le script 'simulate_reboot_script.sh'

		exit
		./simulate_reboot_script.sh
		# Ce script simule une commande système qui est normalement exécutée
		# au redémarrage de la machine ou redémarrage du service tomcat.
		# Cette commande doit être exécuté en temps que root sur la machine. 
		# Vous devrez donc rentrer le mot de passe root du container : 'toor'

6. A la fin du compte à rebours vous pouvez rééssayer de vous connecter en tant que root

		ssh root@localhost -p 4242
		# Si vous essayez avec l'ancien mot de passe cela ne marchera pas
		# Le nouveau mot de passe (injecté grâce à l'exploit) est 'motdepasse'

## Postrequis

N'oubliez pas de tuer votre container docker :

		docker kill cve_tomcat

## Contacts

- [Clément Taboulot](mailto:clement.taboulot@grenoble-inp.org)
- [Vincent Chenal](mailto:vincent.chenal@grenoble-inp.org)
- [Florent Tonneau](florent.tonneau@grenoble-inp.org)
