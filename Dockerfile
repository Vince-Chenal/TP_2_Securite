FROM centos:centos7

RUN yum update -y
RUN yum install -y vim wget rpm-build

# Install Specific Tomcat
WORKDIR /root

# Retrieve tomcat distribution
RUN wget http://vault.centos.org/7.2.1511/os/Source/SPackages/tomcat-7.0.54-2.el7_1.src.rpm

# Resolve build dependencies
RUN yum-builddep -y tomcat-7.0.54-2.el7_1.src.rpm

# Build the RPMs
RUN rpmbuild --rebuild tomcat-7.0.54-2.el7_1.src.rpm

# Remove annoying already installed package
RUN yum remove -y tomcat-servlet-3.0-api

# Install Tomcat distrib
RUN yum install -y /root/rpmbuild/RPMS/noarch/*

# Create tomcat_user and that belongs to tomcat group
RUN useradd tomcat_user --groups tomcat
RUN echo "tomcat_user:motdepasse"|chpasswd

# Change root password
RUN echo "root:toor"|chpasswd

# Install ssh related stuffs
RUN yum -y install openssh-server openssh-clients
RUN ssh-keygen -t rsa -f /etc/ssh/ssh_host_rsa_key -N ''
RUN ssh-keygen -t ecdsa -f /etc/ssh/ssh_host_ecdsa_key -N ''
RUN ssh-keygen -t ed25519 -f /etc/ssh/ssh_host_ed25519_key -N ''

# Copy exploit
COPY exploit.sh /home/tomcat_user/exploit.sh
RUN chown tomcat_user /home/tomcat_user/exploit.sh
RUN chmod u+x /home/tomcat_user/exploit.sh

RUN yum -y install cronie
CMD /usr/sbin/crond -s & /usr/sbin/sshd -D
