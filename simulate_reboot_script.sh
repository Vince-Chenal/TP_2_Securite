#!/bin/bash

ssh root@localhost -p 4242 /usr/bin/systemd-tmpfiles --create
echo "Reboot simulated, cron job that changes the root password will be executed in less than 1 minute"

for i in {1..60}
do
	sleep 1
        echo $i
done

echo "You can now try to connect to root account with the new password \"motdepasse\", the previous one is unuable.\n!! Breach exploited !!"
